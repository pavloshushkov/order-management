from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action

from order_management.core.models import Product, Order
from .serializers import ProductSerializer, OrderSerializer
from .services import product_discount_price_after_days

# TODO: delete
from rest_framework.permissions import AllowAny


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    model = Product
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (AllowAny, )


class OrderViewSet(viewsets.ModelViewSet):
    model = Order
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = (AllowAny, )
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {
        'created_at': ['gte', 'lte'],
    }

    def perform_create(self, serializer):
        product = serializer.validated_data.get('product', False)
        price = serializer.validated_data.get('price', False)
        if product and not price:
            discount_use, set_price, set_price_discount = product_discount_price_after_days(product)
            serializer.save(price=set_price, price_discount=set_price_discount)
        serializer.save()

    @action(methods=['post'], detail=True)
    def create_invoice(self, request, pk):
        order = self.get_object()
        print(f"{order}")
        if order:
            order.generate_invoice()
            return Response(order.invoice, status=status.HTTP_201_CREATED)
        return Response({}, status=status.HTTP_400_BAD_REQUEST)
