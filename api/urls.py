from django.urls import path, include, re_path
from rest_framework import routers


from . import views

router = routers.DefaultRouter()

router.register(r'product', views.ProductViewSet, basename='product')
router.register('order', views.OrderViewSet, basename='order')

urlpatterns = [
    path('api-auth/', include('rest_framework.urls')),
]

urlpatterns += router.urls
