from rest_framework import serializers

from .services import product_discount_price_after_days
from order_management.core.models import Product, Order


class ProductSerializer(serializers.ModelSerializer):
    price_after_discount = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = "__all__"

    def get_price_after_discount(self, obj):
        if isinstance(obj, self.Meta.model):
            discount_use, set_price, set_price_discount = product_discount_price_after_days(obj)
            if discount_use:
                return set_price_discount
        return ''


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = "__all__"
