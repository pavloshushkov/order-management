from decimal import Decimal

DISCOUNT_PRICE_AFTER_DAYS = 30
DISCOUNT_FORMULA = Decimal('0.8')  # Decimal. 20% discount = Decimal('0.8')
