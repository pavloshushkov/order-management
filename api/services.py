from . import constans as const
from django.utils import timezone
from order_management.core.models import Product
from datetime import datetime
from decimal import Decimal
from typing import Tuple


def product_discount_price_after_days(
        product: Product,
        time_now: datetime = timezone.now()) -> Tuple[bool, Decimal, Decimal]:
    if (product.created_at - time_now).days >= const.DISCOUNT_PRICE_AFTER_DAYS:
        discount_use = True
        set_price = product.price * const.DISCOUNT_FORMULA
        set_price_discount = product.price - set_price
    else:
        discount_use = False
        set_price = product.price
        set_price_discount = Decimal('0')
    return discount_use, set_price, set_price_discount
