from django.utils import timezone
from rest_framework.test import APITestCase

from order_management.core.models import Product
from .services import product_discount_price_after_days
from .serializers import ProductSerializer
from . import constans as const

from datetime import timedelta
from decimal import Decimal


class ServicesProductDiscountPriceAfterDaysTest(APITestCase):
    def setUp(self):
        self.product = Product.objects.create(
            name='Product1',
            price=Decimal('10')
        )

    def test_simple_check(self):
        discount_use, set_price, set_price_discount = product_discount_price_after_days(self.product)
        self.assertFalse(discount_use)
        self.assertEqual(set_price, self.product.price)
        self.assertEqual(set_price_discount, Decimal('0'))

    def test_worked(self):
        t = timezone.now() - timedelta(days=const.DISCOUNT_PRICE_AFTER_DAYS, minutes=1)
        discount_use, set_price, set_price_discount = product_discount_price_after_days(
            product=self.product, time_now=t)
        self.assertTrue(discount_use)
        self.assertEqual(
            self.product.price * const.DISCOUNT_FORMULA,
            set_price
        )
        self.assertEqual(
            self.product.price - self.product.price * const.DISCOUNT_FORMULA,
            set_price_discount
        )


class ProductSerializerTest(APITestCase):
    def setUp(self):
        name_p = 'Product1'
        price_p = Decimal('10')
        self.product = Product.objects.create(
            name=name_p,
            price=price_p
        )
        self.data = {
            'name': name_p,
            'price': Decimal('10'),
        }

    def test_default_without_discount(self):
        serializer = ProductSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())
        self.assertEqual(
            float(self.product.price),
            float(serializer.data.get('price'))
        )

