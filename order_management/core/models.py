from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from ..utils import ExtendedEncoder
from decimal import *


class Product(models.Model):
    name = models.CharField(max_length=128)
    price = models.DecimalField(max_digits=10, decimal_places=2, )
    created_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __str__(self):
        return self.name


class Order(models.Model):
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, blank=True, null=True)
    completed = models.BooleanField(default=False)
    paid = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal(0))
    price_discount = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal(0))
    created_at = models.DateTimeField(auto_now=True)

    invoice = models.JSONField(encoder=ExtendedEncoder, default=dict)
    invoice_created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')

    def __str__(self):
        return f"{self.id}"

    def generate_invoice(self):
        invoice_created_at = timezone.now()
        self.invoice = {
            'order_id': self.id,
            'order_creation_date': self.created_at,
            'invoice_creation_date': invoice_created_at,
            'product_id': self.product.id if self.product else '',
            'product_name': self.product.name if self.product else '',
        }
        self.invoice_created_at = invoice_created_at
        self.save()
        return self.invoice
