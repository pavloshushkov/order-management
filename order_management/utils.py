from django.forms import model_to_dict
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Model

from decimal import *
import json


class ExtendedEncoder(DjangoJSONEncoder):
    def default(self, o):

        if isinstance(o, Decimal):
            return float(o)

        return super().default(o)
